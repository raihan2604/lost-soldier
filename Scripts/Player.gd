extends KinematicBody2D

export (int) var speed = 600
export (int) var jump_speed = -600
export (int) var GRAVITY = 1200

const UP = Vector2(0,-1)

var velocity = Vector2()
var kiri = false

func get_input():
	var animation = "idle"
	velocity.x = 0
	if is_on_floor() and Input.is_action_just_pressed('ui_up'):
		velocity.y = jump_speed
		$AudioStreamPlayer2D.play()
		animation = "jump"
	if Input.is_action_pressed('ui_right'):
		velocity.x += speed
		animation = "walk"
		kiri = false
	if Input.is_action_pressed('ui_left'):
		velocity.x -= speed
		animation = "walk"
		kiri = true
		
	if $AnimatedSprite.animation != animation and is_on_floor():
		$AnimatedSprite.play(animation)
	$AnimatedSprite.flip_h = kiri

func _physics_process(delta):
	velocity.y += delta * GRAVITY
	get_input()
	velocity = move_and_slide(velocity, UP)
	
	if is_on_floor() and (Input.is_action_pressed("ui_left") or Input.is_action_pressed("ui_right")):
		$Particles2D.set_emitting(true)
	else:
		$Particles2D.set_emitting(false)


func _on_Stomper_area_entered(area : Area2D):
	if (!area.get_name().begins_with("Checkpoint")):
		velocity.y = -400


func _on_Stomper_body_entered(body : PhysicsBody2D):
	if body.get_name().begins_with("Enemy"):
		Global.lives-=1
		if (Global.lives == 0):
			get_tree().change_scene(str("res://Scenes/GameOver.tscn"))
		else:
			get_tree().change_scene(str("res://Scenes/Main.tscn"))
