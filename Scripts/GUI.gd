extends MarginContainer

var heart1 = load('res://Assets/others/heart1.png')
var heart2 = load('res://Assets/others/heart2.png')
var heart3 = load('res://Assets/others/heart3.png')

func _process(delta):
	if(Global.lives == 3):
		$Sprite.texture=heart3
	if(Global.lives == 2):
		$Sprite.texture=heart2
	if(Global.lives == 1):
		$Sprite.texture=heart1
