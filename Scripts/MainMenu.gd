extends MarginContainer

func _ready():
	$VBoxContainer/HBoxContainer4/MusicSlider.value = Global.current_volume


func _on_PlayButton_pressed():
	get_tree().change_scene(str("res://Scenes/Main.tscn"))


func _on_QuitButton_pressed():
	get_tree().quit()


func _on_MusicSlider_value_changed(value):
	Global.current_volume = value
	if (value==-50):
		AudioServer.set_bus_mute(AudioServer.get_bus_index("Master"), true)
	else:
		AudioServer.set_bus_mute(AudioServer.get_bus_index("Master"), false)
		AudioServer.set_bus_volume_db(AudioServer.get_bus_index('Master'),value)
