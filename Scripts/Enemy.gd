extends KinematicBody2D

var velocity: = Vector2()
const FLOOR_NORMAL: = Vector2.UP
export var max_speed: = Vector2(600.0, 1000.0)
export (int) var GRAVITY = 1200
var kiri = true

func _ready() -> void:
	velocity.x = -max_speed.x

func _physics_process(delta:float) -> void:
	velocity.y += GRAVITY*delta
	if is_on_wall():
		velocity.x *= -1
		kiri = !kiri
		$Sprite.flip_h = kiri
	velocity.y = move_and_slide(velocity,FLOOR_NORMAL).y


func _on_DetectStomp_body_entered(body):
	if body.global_position.y > get_node("DetectStomp/CollisionShape2D").global_position.y:
		return
	get_node("CollisionShape2D").disabled = true
	queue_free()
