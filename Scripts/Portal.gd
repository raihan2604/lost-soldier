extends Node2D

var destination

func _ready():
	destination = get_node("Destination").get_global_position()

func _on_PortalArea_body_entered(body):
	if body.name == "Player":
		body.set_position(destination)
