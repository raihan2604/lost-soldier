extends Area2D

export (String) var sceneName
export (bool) var winScene

func _on_Area_body_entered(body):
	var current_scene = get_tree().get_current_scene().get_name()
	if body.get_name() == "Player":
		if(winScene==true):
			get_tree().change_scene(str("res://Scenes/Win.tscn"))
		else:
			if current_scene == sceneName:
				Global.lives -=1
			if (Global.lives == 0):
				get_tree().change_scene(str("res://Scenes/GameOver.tscn"))
			else:
				get_tree().change_scene(str("res://Scenes/" + sceneName + ".tscn"))
